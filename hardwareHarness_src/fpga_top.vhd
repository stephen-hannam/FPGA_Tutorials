----------------------------------------------------------------------------------
-- fpga_top VHDL
----------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity fpga_top is
	generic (
		G_IN_SIM	  : INTEGER := 0
	);

	port (
			clk_pad   : IN   STD_LOGIC; -- 100Mhz clock
			rst_n     : IN   STD_LOGIC; -- "reset" button input (negative logic)
		--PMOD OLED
			pmod_cs   : OUT  STD_LOGIC;
			pmod_mosi : OUT  STD_LOGIC;
			pmod_sclk : OUT  STD_LOGIC;
			pmod_dc   : OUT  STD_LOGIC;
			pmod_res  : OUT  STD_LOGIC;
			pmod_vbat : OUT  STD_LOGIC;
			pmod_vdd  : OUT  STD_LOGIC;
		--Switches
			btn       : IN   STD_LOGIC_VECTOR(03 downto 00);	-- 4 BUTTONs on FPGA board
			switch    : IN   STD_LOGIC_VECTOR(03 downto 00);	-- 4 SWITCHs on FPGA board
		--LEDs
			led       : OUT	 STD_LOGIC_VECTOR(03 downto 00);	-- 4 LEDs on FPGA board
			led_r     : OUT	 STD_LOGIC_VECTOR(03 downto 00);	-- 4 LEDs on FPGA board -- RED
			led_g     : OUT	 STD_LOGIC_VECTOR(03 downto 00);	-- 4 LEDs on FPGA board -- GREEN
			led_b     : OUT	 STD_LOGIC_VECTOR(03 downto 00)	  -- 4 LEDs on FPGA board -- BLUE
	);
end fpga_top;

architecture RTL of fpga_top is

	component clk_wiz_0
	port
	(
		-- Clock in ports
		clk_in1	: in     STD_LOGIC;
		-- Clock out ports
		clk_100	: out    STD_LOGIC;
		clk_36 	: out    STD_LOGIC
	);
	end component;

	--OLED
	type states_oled is ( Idle, SetupScreen, SendReq, WaitRsp, WRITE_SAMPLE );

	signal state_oled      : states_oled;
	signal state_oled_next : states_oled;

	type r_OLED_SETUP is record
		pos_x  	: INTEGER;
		pos_y 	: INTEGER;
		char  	: std_logic_vector(7 downto 0);
	end record r_OLED_SETUP;

	type ArrayOledSetup		is array (0 to 24) of r_OLED_SETUP;

	--(0,0) => ADC=0x
	--(0,1) => Offset=0x
	--(0,2) =>
	--(0,3) => Weight=X.XXXkg
	constant OledSetupArray : ArrayOledSetup :=
    ( 	( 0,0,X"41"),( 1,0,X"44"),( 2,0,X"43"),( 3,0,X"3D"),( 4,0,X"30"),( 5,0,X"78"),
	  		( 0,1,X"4F"),( 1,1,X"66"),( 2,1,X"66"),( 3,1,X"73"),( 4,1,X"65"),( 5,1,X"74"),( 6,1,X"3D"),( 7,1,X"30"),( 8,1,X"78"),
	  		( 0,3,X"57"),( 1,3,X"65"),( 2,3,X"69"),( 3,3,X"67"),( 4,3,X"68"),( 5,3,X"74"),( 6,3,X"3D"),( 8,3,X"2E"),( 12,3,X"6B"),( 13,3,X"67")
	  );
  -- h'3D -> =

	signal oled_count    : INTEGER;
	signal oled_req      : STD_LOGIC;
	signal oled_req_addr : STD_LOGIC_VECTOR(07 downto 00);
	signal oled_req_data : STD_LOGIC_VECTOR(07 downto 00);
	signal oled_rsp      : STD_LOGIC;
	signal oled_sample   : STD_LOGIC_VECTOR(11 DOWNTO 00);
	signal oled_digit_1  : STD_LOGIC_VECTOR(03 downto 00);
	signal oled_digit_2  : STD_LOGIC_VECTOR(03 downto 00);
	signal oled_digit_3  : STD_LOGIC_VECTOR(03 downto 00);
	signal oled_digit_4  : STD_LOGIC_VECTOR(03 downto 00);


begin

	--led(0) <= ;
	--led(1) <= ;
	--led(2) <= ;
	--led(3) <= ;

	--led_r <= ;
	--led_g <= ;
	--led_b <= ;

PM_PLL : clk_wiz_0
	port map (
	-- Clock in ports
		clk_in1 			=> clk_pad,
	-- Clock out ports
		clk_100  			=> clk_100M
	);

--#################################################################################################
--
-- OLED
--
--#################################################################################################

PM_OLED : entity work.PmodOLEDCtrl
	generic map (
		G_IN_SIM			=> G_IN_SIM
	)
	port map(
		CLK => clk_100M,
		RST => nrst_n,

		CS   => pmod_cs,
		SDIN => pmod_mosi,
		SCLK => pmod_sclk,
		DC   => pmod_dc,
		RES  => pmod_res,
		VBAT => pmod_vbat,
		VDD  => pmod_vdd,

		req      => oled_req,
		req_addr => oled_req_addr,
		req_data => oled_req_data,
		rsp      => oled_rsp
	);

P_SETUP_OLED : process ( clk_100M, rst_n )

		variable hex_digit	: STD_LOGIC_VECTOR(03 downto 00);

	begin
		if ( rst_n = '0' ) then
			state_oled      <= SetupScreen;
			state_oled_next <= SetupScreen;
			oled_count      <= 0;
			oled_sample     <= (others=>'0');

			oled_req      <= '0';
			oled_req_addr <= (others=>'0');
			oled_req_data <= (others=>'0');

		elsif rising_edge( clk_100M ) then

			case state_oled is
				WHEN SetupScreen =>

					oled_req_addr(07 downto 04) <= STD_LOGIC_VECTOR( to_UNSIGNED( OledSetupArray(oled_count).pos_x ,4 ) );
					oled_req_addr(03 downto 00) <= STD_LOGIC_VECTOR( to_UNSIGNED( OledSetupArray(oled_count).pos_y ,4 ) );
					oled_req_data               <= OledSetupArray(oled_count).char;
					state_oled                  <= SendReq;

					if ( oled_count = OledSetupArray'HIGH ) then
						state_oled_next			<= IDLE;
					else
						state_oled_next <= SetupScreen;
						oled_count      <= oled_count + 1;
					end if;

				WHEN IDLE =>
					if ( calc_4 = '1' ) then 				--Do a screen update after every conversion
						state_oled      <= WRITE_SAMPLE;
						state_oled_next <= IDLE;
						oled_count      <= 0;
					end if;


				WHEN WRITE_SAMPLE =>
        --#################################################################################################
        --
        -- OLED customized display outputs
        --
        --#################################################################################################

					--case oled_count is
					--WHEN 0 => 		oled_req_addr(07 downto 04)	<= "0110";		--(6,0)
					--							oled_req_addr(03 downto 00)	<= "0000";
					--							hex_digit	:= STD_LOGIC_VECTOR( xadc_actual(11 downto 08) );

					--WHEN 1 => 		oled_req_addr(07 downto 04)	<= "0111";		--(7,0)
					--							oled_req_addr(03 downto 00)	<= "0000";
					--							hex_digit	:= STD_LOGIC_VECTOR( xadc_actual(07 downto 04) );

					--WHEN 2 => 		oled_req_addr(07 downto 04)	<= "1000";		--(8,0)
					--							oled_req_addr(03 downto 00)	<= "0000";
					--							hex_digit	:= STD_LOGIC_VECTOR( xadc_actual(03 downto 00) );

					--WHEN 3 => 		oled_req_addr(07 downto 04)	<= "1001";		--(9,1)
					--							oled_req_addr(03 downto 00)	<= "0001";
					--							hex_digit	:= STD_LOGIC_VECTOR( xadc_offset(11 downto 08) );

					--WHEN 4 => 		oled_req_addr(07 downto 04)	<= "1010";		--(10,1)
					--							oled_req_addr(03 downto 00)	<= "0001";
					--							hex_digit	:= STD_LOGIC_VECTOR( xadc_offset(07 downto 04) );

					--WHEN 5 => 		oled_req_addr(07 downto 04)	<= "1011";		--(11,1)
					--							oled_req_addr(03 downto 00)	<= "0001";
					--							hex_digit	:= STD_LOGIC_VECTOR( xadc_offset(03 downto 00) );

					--WHEN 6 => 		oled_req_addr(07 downto 04)	<= "0111";		--(7,3)
					--							oled_req_addr(03 downto 00)	<= "0011";
					--							hex_digit	:= oled_digit_1;

					--WHEN 7 => 		oled_req_addr(07 downto 04)	<= "1001";		--(9,3)
					--							oled_req_addr(03 downto 00)	<= "0011";
					--							hex_digit	:= oled_digit_2;

					--WHEN 8 => 		oled_req_addr(07 downto 04)	<= "1010";		--(10,3)
					--							oled_req_addr(03 downto 00)	<= "0011";
					--							hex_digit	:= oled_digit_3;

					--WHEN 9 => 		oled_req_addr(07 downto 04)	<= "1011";		--(11,3)
					--							oled_req_addr(03 downto 00)	<= "0011";
					--							hex_digit	:= oled_digit_4;

					--when others =>	oled_req_addr				<= "11111111";	--Not on screen

					--end case;

					if ( hex_digit <= "1001" ) then
						oled_req_data			<= "0011" & hex_digit;		--hex_digit + 0x30 which is an 0
					else
						hex_digit	:= STD_LOGIC_VECTOR( UNSIGNED(hex_digit) - to_UNSIGNED(9,4) );
						oled_req_data			<= "0100" & hex_digit;		--hex_digit-10 + 0x41 which is an A
					end if;

					state_oled					<= SendReq;

					if ( oled_count = 9 ) then
						state_oled_next			<= IDLE;
					else
						state_oled_next <= WRITE_SAMPLE;
						oled_count      <= oled_count + 1;
					end if;


				WHEN SendReq =>
					oled_req   <= '1';
					state_oled <= WaitRsp;

				WHEN WaitRsp =>
					if ( oled_rsp = '1' ) then
						oled_req   <= '0';
						state_oled <= state_oled_next;
					end if;
			end case;
		end if;

	end process;

end RTL;
