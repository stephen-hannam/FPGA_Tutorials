# UNDER CONSTRUCTION

## FPGA Tutorial Source

### Intended Audience

Those who are starting to move from beginner to intermediate levels of knowledge/skill with FPGA and Hardware Description.

### Assumed Knowledge

1. basic syntax of either VHDL, verilog or both
2. what an FPGA fundamentally is (and is not)
3. what Hardware Description fundamentally is (and is not)
4. basic design workflow followed for simple FPGA designs: RTL -> Simulation -> Synth -> bitstream -> Hardware
5. basic familiarity with a synth-tool/IDE, preferably Vivado

### Resources

NB: the PMOD OLED module used for demonstration requires Vivado 2019.1. I've tried running it with other versions of Vivado, and there seems to be an issue with encryption of some underlying IP cores used. I think the IP cores I use were compiled with some form of encryption enabled, and that encryption is particular to the version of Vivado used.

Where appropriate I reference code snippets or full modules drawn from outside code-bases. One major code-base was the QUT EGH449 Advanced Electronics code-base.

### Covering and Not Covering

Covering: example designs of common types of modules, how they work, test-benching them, running them in hardware, exploring how the synth-tool interprets the Hardware Description

Not-covering: advanced topics like Clock-Domain-Crossing, timing exceptions and other directives to the synth-tool, optimization strategies, etc

In the fashion of NandLand examples with contain a Verilog version and a VHDL version.
